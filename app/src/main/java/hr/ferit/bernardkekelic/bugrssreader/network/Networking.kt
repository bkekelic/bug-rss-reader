@file:Suppress("DEPRECATION")

package hr.ferit.bernardkekelic.bugrssreader.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Networking {

    val showSearchService: BugRss = Retrofit.Builder()
        .addConverterFactory(ConverterFactory.convertFactory)
        .client(HttpClient.client)
        .baseUrl(API_URL)
        .build()
        .create(BugRss::class.java)
}

object ConverterFactory {
    val convertFactory = GsonConverterFactory.create()
}

object HttpClient {
    val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()
}

