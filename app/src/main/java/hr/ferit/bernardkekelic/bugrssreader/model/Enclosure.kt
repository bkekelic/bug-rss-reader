package hr.ferit.bernardkekelic.bugrssreader.model

data class Enclosure (
    val link: String,
    val type: String,
    val length: Int
)