package hr.ferit.bernardkekelic.bugrssreader.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import hr.ferit.bernardkekelic.bugrssreader.R
import hr.ferit.bernardkekelic.bugrssreader.model.SearchResult
import hr.ferit.bernardkekelic.bugrssreader.network.Networking
import hr.ferit.bernardkekelic.bugrssreader.other.NewsResultAdapter
import hr.ferit.bernardkekelic.bugrssreader.model.UsefulSearchResults
import hr.ferit.bernardkekelic.bugrssreader.network.API_KEY
import hr.ferit.bernardkekelic.bugrssreader.network.BUG_URL
import hr.ferit.bernardkekelic.bugrssreader.other.NewsInteractionListener
import hr.ferit.bernardkekelic.bugrssreader.repository.NewsRepository
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoAsyncContext
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), Callback<SearchResult>, AdapterView.OnItemSelectedListener {

    private var selectedCategory = "Naslovna"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpRecyclerView()
        setUpSpinner()
        setUpUi()
        getNews()
    }

    private fun setUpSpinner() {

        val spinner: Spinner = findViewById(R.id.searchCategory)
        spinner.onItemSelectedListener = this

        ArrayAdapter.createFromResource(
            this,
            R.array.categories_array,
            android.R.layout.simple_spinner_item
        ).also {adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedCategory = parent?.getItemAtPosition(position).toString()
        Log.d("TAG", selectedCategory)
    }

    private fun setUpUi() {

        searchButton.setOnClickListener { getNews() }
    }

    private fun setUpRecyclerView() {
        newsFound.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        newsFound.addItemDecoration( DividerItemDecoration(this, RecyclerView.VERTICAL))
        newsFound.itemAnimator = DefaultItemAnimator()
    }

    private fun getNews() {
        if(selectedCategory == "Naslovna") selectedCategory = ""
        var url = BUG_URL + selectedCategory + "&api_key=" + API_KEY
        Networking.showSearchService.findNews(url).enqueue(this)
    }

    override fun onFailure(call: Call<SearchResult>, t: Throwable) {
        Toast.makeText(this, getString(R.string.GetNewsFailure_Text), Toast.LENGTH_SHORT).show()
    }

    override fun onResponse(call: Call<SearchResult>, response: Response<SearchResult>) {
        val results: SearchResult? = response.body()
        storeUsefulSearchResults(results)
        displayData()
        Toast.makeText(this, getString(R.string.GetNewsSuccess_Text), Toast.LENGTH_SHORT).show()
    }

    private fun displayData() {

        val newsListener = object: NewsInteractionListener{
            override fun onOpenNews(link: String) {
                intent =  Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(intent)
            }
        }
        newsFound.adapter = NewsResultAdapter(NewsRepository.news, newsListener)
    }

    private fun storeUsefulSearchResults(result: SearchResult?) {

        removeAllElementsFromList()
        for(i in 0 until result!!.items.size){
            NewsRepository.news.add(UsefulSearchResults(
                i,
                result.items[i].title,
                result.items[i].content,
                result.items[i].pubDate,
                result.items[i].link,
                result.items[i].enclosure.link
                )
            )
        }
    }

    private fun removeAllElementsFromList() {
        for(i in 0..NewsRepository.news.size){
            NewsRepository.remove(i)
        }
    }

}
