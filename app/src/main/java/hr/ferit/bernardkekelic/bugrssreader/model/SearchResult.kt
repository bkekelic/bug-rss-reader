package hr.ferit.bernardkekelic.bugrssreader.model

data class SearchResult (
    val status: String,
    val feed: Feed,
    val items: List<Item>
    )

