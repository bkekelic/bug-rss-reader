package hr.ferit.bernardkekelic.bugrssreader.other

interface NewsInteractionListener {
    fun onOpenNews(link: String)
}