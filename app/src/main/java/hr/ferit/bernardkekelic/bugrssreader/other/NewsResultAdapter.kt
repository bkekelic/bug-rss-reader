package hr.ferit.bernardkekelic.bugrssreader.other

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import hr.ferit.bernardkekelic.bugrssreader.R
import hr.ferit.bernardkekelic.bugrssreader.model.UsefulSearchResults
import kotlinx.android.synthetic.main.item_news_result.view.*

class NewsResultAdapter(
    results: MutableList<UsefulSearchResults>,
    newsListener: NewsInteractionListener
): RecyclerView.Adapter<NewsResultHolder>() {

    private val results: MutableList<UsefulSearchResults>
    private val newsListener: NewsInteractionListener

    init {
        this.results = mutableListOf()
        this.results.addAll(results)
        this.newsListener = newsListener
    }
    fun refreshData(newResult: List<UsefulSearchResults>){
        results.clear()
        results.addAll(newResult)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsResultHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_news_result, parent, false)
        return NewsResultHolder(view)
    }

    override fun getItemCount(): Int = results.size

    override fun onBindViewHolder(viewHolder: NewsResultHolder, position: Int) {
       viewHolder.bind(results.get(position), newsListener)
    }
}

class NewsResultHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    fun bind(result: UsefulSearchResults, newsListener: NewsInteractionListener) {
        itemView.item_title.text = result.title
        itemView.item_content.text = result.content
        itemView.item_date.text = result.publishDate

        val url = result.imageUrl
        Picasso.get()
            .load(url)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(itemView.item_image)

        itemView.setOnClickListener { newsListener.onOpenNews(result.link) }
    }

}
