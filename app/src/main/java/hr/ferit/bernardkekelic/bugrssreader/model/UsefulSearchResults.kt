package hr.ferit.bernardkekelic.bugrssreader.model

data class UsefulSearchResults(
    var id: Int = 0,
    var title: String = "",
    var content: String = "",
    var publishDate: String = "",
    var link: String = "",
    var imageUrl: String = ""
)