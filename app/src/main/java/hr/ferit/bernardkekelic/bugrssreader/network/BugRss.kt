package hr.ferit.bernardkekelic.bugrssreader.network

import hr.ferit.bernardkekelic.bugrssreader.model.SearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

const val API_URL = "https://api.rss2json.com/v1/"
const val BUG_URL = "api.json?rss_url=https://www.bug.hr/rss/"
const val API_KEY = "qot1ru2pwvlnp1dc86tk0xmosuvzzsu8wpweo5qz"

interface BugRss {

    @GET
    fun findNews(@Url path: String): Call<SearchResult>
}