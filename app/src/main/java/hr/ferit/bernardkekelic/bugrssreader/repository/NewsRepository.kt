package hr.ferit.bernardkekelic.bugrssreader.repository

import hr.ferit.bernardkekelic.bugrssreader.model.UsefulSearchResults

object NewsRepository {

    val news: MutableList<UsefulSearchResults> = mutableListOf()

    fun add(item: UsefulSearchResults) = news.add(item)
    fun get(id: Int): UsefulSearchResults? = news.find { items -> items.id == id }

    fun remove(id: Int) = news.removeAll{ items -> items.id == id }
}